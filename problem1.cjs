const path = require("path");
const fs = require("fs");

function createAndDeleteJSONFiles(numberOfFilesCreated) {
  const directoryPath = path.join(__dirname, "./random");

  const promise = createDirectory();
  promise
    .then(function (message) {
      console.log(message);
      return writeFiles();
    })
    .then(function (message) {
      console.log(message);
      return deleteFiles();
    })
    .then(function (message) {
      console.log(message);
    })
    .catch(function (err) {
      console.error(new Error(err));
    });

  function createDirectory() {
    return new Promise(function (resolve, reject) {
      fs.mkdir(directoryPath, { recursive: true }, (err) => {
        if (err) {
          reject(err);
        } else {
          resolve("Directory is made");
        }
      });
    });
  }

  function writeFiles() {
    return new Promise(function (resolve, reject) {
      let allPromises = [];

      let arr = new Array(numberOfFilesCreated).fill(0);

      allPromises = arr.map((currElement, index) => {
        let fileName = `random${index + 1}.json`;
        return createEachFile(fileName);
      });

      function createEachFile(fileName) {
        return new Promise(function (resolve, reject) {
          fs.writeFile(
            path.join(directoryPath, fileName),
            JSON.stringify({ carName: "taigun", brand: "volkswagen" }),
            "utf8",
            (err) => {
              if (err) {
                reject(err);
              } else {
                resolve(`File ${fileName} is written`);
              }
            }
          );
        });
      }

      Promise.all(allPromises)
        .then(function (response) {
          resolve(response);
        })
        .catch(function (err) {
          reject(err);
        });
    });
  }

  function deleteFiles() {
    return new Promise(function (resolve, reject) {
      let allPromises = [];

      let arr = new Array(numberOfFilesCreated).fill(0);

      allPromises = arr.map((currElement, index) => {
        let fileName = `random${index + 1}.json`;
        return deleteEachFile(fileName);
      });

      function deleteEachFile(fileName) {
        return new Promise(function (resolve, reject) {
          fs.unlink(path.join(directoryPath, fileName), (err) => {
            if (err) {
              reject(err);
            } else {
              resolve(`File ${fileName} is deleted`);
            }
          });
        });
      }

      Promise.all(allPromises)
        .then(function (response) {
          resolve(response);
        })
        .catch(function (err) {
          reject(err);
        });
    });
  }
}

module.exports = createAndDeleteJSONFiles;
