const path = require("path");
const fs = require("fs");

function convertToUpperCaseLowerCase() {
  let directory = __dirname;

  const promise = readFile("./lipsum.txt");
  promise //  Read the given file lipsum.txt and Convert the content to uppercase & write to a new file and append name of the new file in filenames.txt
    .then(function (data) {
      console.log("File Read Successfully");
      let dataInUpperCase = data.toUpperCase();
      return writeFile(dataInUpperCase, "lipsumUpper.txt");
    })
    .then(function (fileName) {
      console.log(`${fileName} written successfuly`);
      return appendFile(fileName);
    })
    .then(function (message) {
      console.log(message);
      return readFile("lipsumUpper.txt");
    }) //  Read the new file and convert it to lower case. Then split the contents into sentences.
    .then(function (data) {
      console.log("File Read Successfully");
      let dataInLowerCase = data
        .toLowerCase()
        .replaceAll("\n\n", " ")
        .split(". ")
        .join("\n");
      dataInLowerCase = dataInLowerCase.slice(0, dataInLowerCase.length - 1);
      return writeFile(dataInLowerCase, "lipsumLower.txt");
    })
    .then(function (fileName) {
      console.log(`${fileName} written successfuly`);
      return appendFile(fileName);
    })
    .then(function (message) {
      console.log(message);
      return readFile("./lipsumLower.txt");
    }) // Read the new files, sort the content, write it out to a new file and append the name of the new file in filenames.txt
    .then(function (data) {
      console.log("File Read Successfully");
      let dataToBeSorted = data.split("\n");

      let sortedData = dataToBeSorted.sort().toString();
      return writeFile(sortedData, "lipsumSorted.txt");
    })
    .then(function (fileName) {
      console.log(`${fileName} written successfuly`);
      return appendFile(fileName);
    })
    .then(function (message) {
      console.log(message);
      return readFile("./filenames.txt");
    }) // Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
    .then(function (data) {
      console.log("File Read Successfully");
      let filesArray = data.split("\n");
      filesArray = filesArray.slice(0, filesArray.length - 1);
      return deleteFiles(filesArray, "lipsumSorted.txt");
    })
    .then(function (message) {
      console.log(message);
    })
    .catch(function (err) {
      console.error(new Error(err));
    });

  function readFile(fileName) {
    return new Promise(function (resolve, reject) {
      fs.readFile(path.join(directory, fileName), "utf-8", (err, data) => {
        if (err) {
          reject(err);
        } else {
          resolve(data);
        }
      });
    });
  }

  function deleteFiles(filesArray) {
    return new Promise(function (resolve, reject) {
      let allPromises = [];
      allPromises = filesArray.map((fileName) => {
        return deleteEachFile(fileName);
      });

      function deleteEachFile(fileName) {
        return new Promise(function (resolve, reject) {
          fs.unlink(path.join(directory, fileName), (err) => {
            if (err) {
              reject(err);
            } else {
              resolve(`File ${fileName} is deleted`);
            }
          });
        });
      }

      Promise.all(allPromises)
        .then(function (response) {
          resolve(response);
        })
        .catch(function (err) {
          reject(err);
        });
    });
  }

  function writeFile(data, fileName) {
    return new Promise(function (resolve, reject) {
      fs.writeFile(
        path.join(directory, fileName),
        data,
        "utf-8",
        function (err) {
          if (err) {
            reject(err);
          } else {
            resolve(fileName);
          }
        }
      );
    });
  }

  function appendFile(fileName) {
    return new Promise(function (resolve, reject) {
      fs.appendFile(
        path.join(directory, "filenames.txt"),
        `${fileName}` + "\n",
        "utf-8",
        function (err) {
          if (err) {
            reject(err);
          } else {
            resolve("filenames.txt appended successfully");
          }
        }
      );
    });
  }
}

module.exports = convertToUpperCaseLowerCase;
